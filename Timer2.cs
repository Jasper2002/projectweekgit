﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Timer2 : MonoBehaviour
{
    // Dit is voor the timer in de game, na 20 seconden is de tijd om.
    private float startTime;
    public Text timerText;
    public float timeRemaining = 20;

        void Start () {
            startTime = Time.time;
        }

        void Update () {
            float t = Time.time - startTime;

            string minutes = ((int) t / 60) . ToString();
            string seconds = (t % 60) . ToString("f0");

            timerText.text = minutes + ":" + seconds;

            if (timeRemaining > 0)
                        {
                            timeRemaining -= Time.deltaTime;
                            Debug.Log("Time left:" +  " " + timeRemaining);
                        }
                        else
                        {
                            Debug.Log("Time has run out!");
                            Time. timeScale = 0;
                        }
        }
}
    